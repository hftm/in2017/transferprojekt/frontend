FROM socialengine/nginx-spa
COPY dist/ /app
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint
ENTRYPOINT ["docker-entrypoint"]
CMD ["nginx"]

import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/creategame',
      name: 'creategame',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/CreateGame.vue')
    },
    {
        path: '/startgame',
        name: 'startgame',
        component: () => import('./views/StartGame.vue')
    },
    {
        path: '/joingame',
        name: 'joingame',
        component: () => import('./views/JoinGame.vue')
    },
    {
        path: '/createbuzzwordcard',
        name: 'createbuzzwordcard',
        component: () => import('./views/CreateBuzzwordCard.vue')
    },
    {
        path: '/playGame',
        name: 'playGame',
        component: () => import('./views/PlayGame.vue')
    },
    {
      path: '/CardGridExample',
      name: 'CardGridExample',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/CardGridExample.vue')
    },
    {
        path: '/StatusExample',
        name: 'StatusExample',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ './views/StatusExample.vue')
      }
  ]
})

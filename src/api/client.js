import axios from 'axios';

axios.defaults.baseURL = process.env.VUE_APP_ENDPOINT_URL;

export default {
    createGame({ 
        gameDuration, // int gameDuration
        gameName, // String gameName
        gameStart, // JsonFormat(shape = JsonFormat.Shape.STRING, pattern="2019-12-22T15:14:03.947Z")
        gamelevel, // int level { easy = 0, heavy = 1, medium = 2 }
        needFullCard, // boolean
        nickName // String nickName 
    }                
    ) {
        return axios.post(
            '/newGame',
            {
                gameDuration: gameDuration,
                gameName: gameName,
                gameStart: gameStart, 
                gamelevel: gamelevel,
                needFullCard: needFullCard,
                nickName: nickName,
            });
    },
    joinGame({ gameCode, nickName }) {
        return axios.post('/joinGame',
            {
                gameCode: gameCode,
                nickName: nickName
            });
    },
    startGame(gameCode, nickName, cards) {
        return axios.post('/createCard', {gameCode: gameCode, nickName: nickName, cards: cards});
    },
    createCard({gameCode, nickName, card}) {
        return axios.post('/createCard', { gameCode: gameCode, nickName: nickName, card: card});
    },
    buzzWordHit({ gameCode, nickName, buzzword }) {
        return axios.post('/buzzWordHit', {
            gameCode: gameCode,
            nickName: nickName,
            buzzword: buzzword
        });
    },
    winnerResults(gameCode) {
        return axios.post('/winnerResults', {gameCode: gameCode});
    },
    acceptWinner(gameCode, nickName) {
        return axios.post('/winnerResultsAccepted', {gameCode: gameCode, nickName: nickName})
    },
    declineWinner(gameCode, nickName) {
        return axios.post('/winnerResultsNotAccepted', {gameCode: gameCode, nickName: nickName})
    }
};
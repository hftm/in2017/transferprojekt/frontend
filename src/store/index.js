import Vue from 'vue'
import Vuex from 'vuex'
import ClientApi from '@/api/client';
import {MUTATIONS} from "@/constants/mutations";
import Difficulty from '@/constants/difficulty';
import GameModule from './modules/game';
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        game: GameModule,
    },
    state: {
        isLoading: false,
        errors: []
    },
    getters: {
        isLoading: (state) => state.isLoading,
        getErrors: (state) => state.errors
    },
    mutations: {
        [MUTATIONS.RESET](state) {
            state.isLoading = false;
            state.errors = [];
        },
        [MUTATIONS.LOAD](state) {
            state.isLoading = true;
        },
        [MUTATIONS.FINISH_LOAD](state) {
            state.isLoading = false;
        },
        [MUTATIONS.ERROR](state, data) {
            state.errors.push(data);
        }
    },
    actions: {
        reset: function ({dispatch, commit}) {
            commit(MUTATIONS.RESET);
            dispatch('game/reset');
        },
        createGame: function ({dispatch, commit}, { gameDuration, gameName, gameStart, gamelevel, needFullCard, nickName }) {
            commit(MUTATIONS.LOAD);
            return ClientApi.createGame({ gameDuration, gameName, gameStart, gamelevel, needFullCard, nickName })
                .then(res => {
                    let data = res.data;
                    data.nickName = nickName;
                    data.startDate = gameStart;
                    data.level = gamelevel;
                    data.duration = gameDuration
                    dispatch('game/createGame', data);
                })
                .catch(err => {
                    commit(MUTATIONS.ERROR, err.response.data.message);
                })
                .finally(() => commit(MUTATIONS.FINISH_LOAD));
        },
        joinGame: function ({dispatch, commit}, { gameCode, nickName }) {
            return ClientApi.joinGame({ gameCode, nickName })
                .then(res => {
                    let data = res.data;
                    data.nickName = nickName;
                    dispatch('game/joinGame', data);
                })
                .catch(err => {
                    commit(MUTATIONS.ERROR, err.response.data.message)
                    throw err
                })
                .finally(() => commit(MUTATIONS.FINISH_LOAD));
        },
        createCards: function ({dispatch, commit}, { gameCode, nickName, card }) {
            commit(MUTATIONS.LOAD);
            let gameData = {
                card: card.buzzWords
            }
            return ClientApi.createCard({ gameCode, nickName, card })
                .then(res => {
                    dispatch('game/initGame', gameData);
                })
                .catch(err => {
                    commit(MUTATIONS.ERROR, err.response.data.message)
                    throw err
                })
                .finally(() => commit(MUTATIONS.FINISH_LOAD));
        },
        hitBuzzword: function ({dispatch, commit, rootGetters}, buzzword ){
            commit(MUTATIONS.LOAD);
            let gameCode = rootGetters['game/getGameCode']
            let nickName = rootGetters['game/getNickName']
            return ClientApi.buzzWordHit({ gameCode, nickName, buzzword })
                .then(res => {
                    let data = res.data;
                    if (data.winner || data.winnerNickName != null){
                        dispatch('game/finishGame', data);
                    }
                })
                .catch(err => {
                    commit(MUTATIONS.ERROR, err.response.data.message)
                    throw err
                })
                .finally(() => commit(MUTATIONS.FINISH_LOAD));
        }
    }
});
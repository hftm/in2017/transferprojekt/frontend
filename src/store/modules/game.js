import {MUTATIONS} from "@/constants/mutations";
import {GAME_STATES} from "@/constants/game-states";
import {GAME_LEVEL} from "@/constants/game-level";

export default {
    namespaced: true,
    state: {
        runningState: GAME_STATES.NOT_RUNNING,
        gameMaster: false,
        name: "",
        nickName: "",
        code: "",
        start: "",
        duration: "",
        level: GAME_LEVEL.EASY,
        players: [],
        cards: [],
        winner: ""
    },
    getters: {
        getRunningState: (state) => state.runningState,
        isGameMaster: (state) => state.gameMaster,
        getGameName: (state) => state.name,
        getNickName: (state) => state.nickName,
        getGameCode: (state) => state.code,
        getStart: (state) => state.start,
        getDuration: (state) => state.duration,
        getLevel: (state) => state.level,
        getPlayers: (state) => state.players,
        getPlayerByName: (state) => (playerName) => state.players.find((player) => player.name === playerName),
        getCards: (state) => state.cards,
        getWinner: (state) => state.winner
    },
    mutations: {
        [MUTATIONS.RESET](state) {
            state.runningState = GAME_STATES.NOT_RUNNING;
            state.gameMaster = false;
            state.name = "";
            state.nickName = "";
            state.code = "";
            state.start = "";
            state.duration = "";
            state.level = 0;
            state.players = [];
            state.cards = [];
            state.winner = "";
        },
        [MUTATIONS.CREATE_GAME](state) {
            state.gameMaster = true;
        },
        [MUTATIONS.START_GAME](state) {
            state.runningState = GAME_STATES.RUNNING;
        },
        [MUTATIONS.JOIN_GAME](state, data) {
            state.runningState = GAME_STATES.INIT_GAME;
            state.name = data.gameName;
            state.nickName = data.nickName;
            state.code = data.gameCode;
            state.start = data.startDate;
            state.duration = data.duration;
            state.level = data.level;
        },
        [MUTATIONS.INIT_GAME](state, data) {
            state.runningState = GAME_STATES.WAITING;
            state.cards = data.card;
        },
        [MUTATIONS.BUZZWORD_HIT](state, data) {
            state.cards.find((card) => (card.id === data.id)).counter++;
        },
        [MUTATIONS.GAME_FINISH_VALIDATE_RESULT](state, data) {
            state.runningState = GAME_STATES.FINISHED_VALIDATE_RESULT;
            state.winner = data.winnerNickName;
        },
        [MUTATIONS.GAME_FINISH](state, data) {
            state.runningState = GAME_STATES.FINISHED;
            state.winner = data.winnerNickName;
        },
    },
    actions: {
        reset: function({commit}) {
            commit(MUTATIONS.RESET);
        },
        createGame: function ({ dispatch, commit }, data) {
            commit(MUTATIONS.RESET);
            commit(MUTATIONS.CREATE_GAME);
            dispatch('joinGame', data);
        },
        joinGame: function ({commit}, data) {
            commit(MUTATIONS.RESET);
            commit(MUTATIONS.JOIN_GAME, data);
        },
        startGame: function({commit}, data) {
            commit(MUTATIONS.START_GAME);
        },
        initGame: function({commit}, data) {
            commit(MUTATIONS.INIT_GAME, data);
        },
        finishGame: function ({commit, getters}, data) {
            commit(MUTATIONS.GAME_FINISH_VALIDATE_RESULT, data);
        },
        validateWinner: function ({commit}, data) {
            commit(MUTATIONS.GAME_FINISH, data);
        }
    },
}
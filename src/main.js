import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import App from './App.vue'
import router from './router'
import store from './store'

import '@/assets/styles/custom.scss'
import VueConfetti from 'vue-confetti';

Vue.use(VueConfetti);

library.add(fas)

Vue.use(BootstrapVue)
Vue.use(VueMomentJS, moment)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
